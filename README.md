# Open Refine Recipes



## Name
Open Refine recipes for my team

## Description
In this repo you'll find mostly clustering jsons that have been used to clean Wikimedia Culture & Heritage related datasets

## Usage
You'll be most likely interested if you have been clustering / cleaning data on GLAM Wiki related issues

## Support
I am not actively supporting the development of this but I'm happy [to take questions](https://meta.wikimedia.org/wiki/User:SEgt-WMF)

## Authors and acknowledgment
Silvia E. Gutiérrez De la Torre

## License
CC BY 4.0
